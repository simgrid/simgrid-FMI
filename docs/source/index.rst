.. simgrid-FMI documentation master file

.. _index:


SimGrid-FMI
=======================================

`SimGrid <https://simgrid.org>`_ plugin based on the `FMI standard <https://fmi-standard.org/>`_ to co-simulate distributed IT infrastructures and continuous multi-physical systems.
For more information, please refer to this article: `Co-simulation of FMUs and Distributed Applications with SimGrid <https://hal.science/hal-01762540>`_.

.. toctree::
   :hidden:
   :maxdepth: 1
   :caption: Tutorials:

	Sinus <sinus.rst>

.. toctree::
   :hidden:
   :maxdepth: 1
   :caption: User Manual:

    Installing SimGrid-FMI <installation.rst>
    Using SimGrid-FMI <usage.rst>


