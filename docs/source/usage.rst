.. _usage:

Using SimGrid-FMI
======================

.. role:: cpp(code)
  :language: cpp
  :class: highlight

init
""""

.. code-block:: cpp

    void init(double communication_step)

Initialize the FMI plugin of SimGrid.
This method has to be called after starting the SimGrid engine.
The other SimGrid-FMI methods can not be called before this one.
The SimGrid-FMI co-simulation master will manage the FMUs with an integration step-size and a state-events detection precision equal to :cpp:`communication_step`.

add_fmu_cs
""""""""""

.. code-block:: cpp

    void add_fmu_cs(const std::string& fmu_uri, const std::string& fmu_name)

Add a new FMU :cpp:`fmu_name` located at :cpp:`fmu_uri` to be co-simulated with SimGrid --i.e. load and initialize the FMU.
SimGrid will manage the co-evolution of the FMU with its own models and the distributed application.
SimGrid actors can then interact with the FMU.

connect_fmu
"""""""""""

.. code-block:: cpp

    void connect_fmu(const std::string& out_fmu_name, const std::string& output_port, const std::string& in_fmu_name, const std::string& input_port);

Add an output-to-input ports connection between two FMUs.
During the co-simulation, SimGrid will update the :cpp:`input_port` value of :cpp:`out_fmu_name` according to the :cpp:`output_port` value of :cpp:`in_fmu_name`.
Input ports connected to other FMUs can not be set manually by SimGrid Actors.

ready_for_simulation
""""""""""""""""""""

.. code-block:: cpp

    void ready_for_simulation();

Solve the initial FMUs couplings.
Have to be called before starting the SimGrid simulation.
New couplings are forbidden after calling this method.

register_event
""""""""""""""

.. code-block:: cpp

    void register_event(const std::function<bool(const std::vector<std::string>&)>& condition, const std::function<void(const std::vector<std::string>&)>& handleEvent, const std::vector<std::string>& params);

Define state-event and register a callback.
The :cpp:`condition` boolean function determines (i.e. returns true) when the state-event occurs (e.g. return x >= 0).
The :cpp:`handle_event` function will be called by the master with the :cpp:`params` parameters when the state-event will occur.
Each state-event is triggered only once.

delete_events
"""""""""""""

.. code-block:: cpp

    void delete_events();

Delete all the previously registered state-events.
The co-simulation master will stop detecting these state-events. Then no callbacks will be called.

connect_<TYPE>_to_simgrid
"""""""""""""""""""""""""

.. code-block:: cpp

    void connect_<TYPE>_to_simgrid(const std::function<<TYPE>(std::vector<std::string>)>& generateInput,
                             const std::vector<std::string>& params, const std::string& fmu_name,
                             const std::string& input_name);


Connect the SimGrid models to the real input port (i.e. continuous) :cpp:`input_name` of :cpp:`fmu_name`.
The :cpp:`generate_input` function is used with :cpp:`params` to convert the state of SimGrid models into FMU input port values.
The SimGrid-FMI master will automatically use this function to update the FMU input port upon SimGrid models state change.

get_var_type
""""""""""""

.. code-block:: cpp

    var_type get_var_type(const std::string& fmu_name, const std::string& var_name);

Get the type (i.e. :cpp:`real`, :cpp:`integer`, :cpp:`boolean`, :cpp:`string` or :cpp:`unknown`) of an FMU variable.


get_<TYPE>
""""""""""

.. code-block:: cpp

    double get_<TYPE>(const std::string& fmi_name, const std::string& output_name);

Return the current value of the output port (i.e. continuous) :cpp:`output_name` of :cpp:`fmi_name`.
Available types are :cpp:`real`, :cpp:`boolean`, :cpp:`integer` and :cpp:`string`.

set_<TYPE>
""""""""""

.. code-block:: cpp

    void set_<TYPE>(const std::string& fmi_name, const std::string& input_name, TYPE value);

Set the :cpp:`value` of the input port (i.e. continuous) :cpp:`input_name` of :cpp:`fmi_name`.
Input ports connected to other FMUs can not be set using this method. Available types are :cpp:`real`, :cpp:`boolean`, :cpp:`integer` and :cpp:`string`.






