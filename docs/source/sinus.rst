.. _sinus:

Sinus
======================================

.. role:: cpp(code)
  :language: cpp
  :class: highlight

In this example, we consider a simple FMU with an output variable x=sin(time). 
We test that we can properly detect and trigger event when x cross the x = 0 threshold.
This example can be found in the `framagit repository <https://framagit.org/simgrid/simgrid-FMI/-/blob/master/examples/sinus/s4u-sinus.cpp>`_  of the SimGrid-FMI project, among other examples.


Includes
""""""""

Before anything, we need to include the necessary modules.

.. code-block:: cpp

    #include "simgrid/s4u.hpp"
    #include "simgrid-fmi.hpp"
    #include <math.h>

Main function
"""""""""""""

Instantiate the SimGrid Engine.

.. code-block:: cpp

    simgrid::s4u::Engine e(&argc, argv);


Initialize the SimGrid-FMI plugin. :cpp:`step_size` is defined by a global variable: :cpp:`const double step_size = 0.001;`.

.. code-block:: cpp

    simgrid::fmi::init(step_size);

Load the platform inside the SimGrid Engine.

.. code-block:: cpp

    e.load_platform("../../platforms/clusters_rennes.xml");

Register the FMU. :cpp:`fmu_name` is defined by a global variable: :cpp:`const std::string fmu_name = "sinus";`.

.. code-block:: cpp

    std::string fmu_uri = (argc>=2)? argv[1] : "sinus_fmu/sinus.fmu";
    simgrid::fmi::add_fmu_cs(fmu_uri, fmu_name);

Lock SimGrid-FMI parameters.

.. code-block:: cpp

    simgrid::fmi::ready_for_simulation();

Register the event we want to track. Both :cpp:`reactOnPositiveValue` and :cpp:`testEventTime` are described in the next sections.

.. code-block:: cpp

    simgrid::fmi::register_event(reactOnPositiveValue, testEventTime, {});

Run the SimGrid simulation. :cpp:`max_sim_time` is defined by a global variable: :cpp:`const double max_sim_time = (expected_detections-1) * M_PI + 1;`.

.. code-block:: cpp

    e.run_until(max_sim_time);

Event detection
"""""""""""""""

This first function returns true if x goes below 0.

.. code-block:: cpp

    static bool reactOnNegativeValue(const std::vector<std::string>&){
        return simgrid::fmi::get_real(fmu_name, "x") < 0;
    }

This second function returns true if the :cpp:`positive` variable of the FMU equals 1.

.. code-block:: cpp

    static bool reactOnPositiveValue(const std::vector<std::string>&){
        return simgrid::fmi::get_integer(fmu_name,"positive") == 1;
    }

Event handling
""""""""""""""

The event handler reacts whenever x crosses the x = 0 threshold. It counts the number of occurence and register the next event to track.


.. code-block:: cpp

    static void testEventTime(const std::vector<std::string>&)
    {
        double actual_time = simgrid::s4u::Engine::get_clock();
        double expected_time = nb_event * M_PI;
        nb_event++;

        if (nb_event % 2 !=0)
            simgrid::fmi::register_event(reactOnNegativeValue, testEventTime, {});
        else
            simgrid::fmi::register_event(reactOnPositiveValue, testEventTime, {});
    }


