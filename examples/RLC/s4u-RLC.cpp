#include "simgrid/s4u.hpp"
#include "simgrid-fmi.hpp"
#include <string>
#include <iostream>
#include <fstream>

XBT_LOG_NEW_DEFAULT_CATEGORY(main, "Messages specific for this msg example");

// SYSTEM'S STATE

const double max_sim_time = 15;
const double sampling_time = 0.01;

// UTILITY

static void checkResults(){
  double t = simgrid::s4u::Engine::get_clock();
  double i = simgrid::fmi::get_real("RLC","i");
  double uc = simgrid::fmi::get_real("RLC","Uc");
  double ul = simgrid::fmi::get_real("RLC","Ul");
  double ur = simgrid::fmi::get_real("RLC","Ur");
  std::ofstream out ("output.csv", std::ios::app);
  out << t << ";" << uc << ";" << i << ";" << ul << ";" << ur << "\n";
  out.close();
}

static void sampler()
{
  XBT_INFO("perform simulation of RLC system");
  checkResults();
  while(simgrid::s4u::Engine::get_clock() < max_sim_time){
    simgrid::s4u::this_actor::sleep_for(sampling_time);
    checkResults();
  }
  XBT_INFO("co-simulation of RLC system done ! see you !");
}

// MAIN

int main(int argc, char *argv[])
{
  std::ofstream out ("output.csv", std::ios::out);
  out << "\"time\",\"Uc\",\"i\",\"Ul\",\"Ur\"\n";
  out.close();

  // SIMGRID INIT
  simgrid::s4u::Engine e(&argc, argv);


  const double intstepsize = 1;
  simgrid::fmi::init(intstepsize);

  e.load_platform("../../platforms/clusters_rennes.xml");

  // ADDING FMUs

  std::string fmu_uri = (argc >= 2)? argv[1] : "RLC_FMU/RLC.fmu";

  std::string fmu_name = "RLC";
  simgrid::fmi::add_fmu_cs(fmu_uri, fmu_name);

  simgrid::fmi::ready_for_simulation();

  // CREATING SIMGRID ACTORS

  simgrid::s4u::Actor::create("sampling_actor", simgrid::s4u::Host::by_name("c-0.rennes"), sampler);

  e.run();
}
