model RLC
  parameter Real R = 1;
  parameter Real L = 1;
  parameter Real C = 0.1;
  parameter Real U = 10;
  Real Ul;
  Real Ur;
  Real i(start = 0);
  Real Uc(start = 0);
equation
  i = C * der(Uc);
  U = Ur + Ul + Uc;
  Ul = L * der(i);
  Ur = R * i;
end RLC;
