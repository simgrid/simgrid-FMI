#!/bin/bash

cd examples/battery

# we run the FMU simulation of the battery example.
battery="$(find `pwd` -name battery.fmu)"
./s4u-battery "$battery"

res="$?"

if [ "$res" -ne "0" ]
then
        echo "error: the SimGrid simulation did not end properly"
        exit 1
fi
