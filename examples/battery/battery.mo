model battery
  output Real E;
  output Real Pout;
  input Real Pneed;

initial equation
  E = 8000000; 

equation
  if(E > 0) then
    der(E) = -Pneed;
    Pout = Pneed;
  else
    der(E) = 0;
    Pout = 0;
  end if;

end battery;
