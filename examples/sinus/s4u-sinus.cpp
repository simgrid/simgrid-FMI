#include "simgrid/s4u.hpp"
#include "simgrid-fmi.hpp"
#include <math.h>

XBT_LOG_NEW_DEFAULT_CATEGORY(main, "Messages specific for this test example");

/**
* In this example, we consider a simple FMU with an output variable x=sin(time).
* We test that we can properly detect and trigger event when x cross the x = 0 threshold.
*/

const std::string fmu_name = "sinus";
const double step_size = 0.001;
const double error = 0.0001;
const int expected_detections = 100;
const double max_sim_time = (expected_detections-1) * M_PI + 1;

/**
 * Event detection functions
 */

static bool reactOnNegativeValue(const std::vector<std::string>&){
  return simgrid::fmi::get_real(fmu_name, "x") < 0;
}

static bool reactOnPositiveValue(const std::vector<std::string>&){
  return simgrid::fmi::get_integer(fmu_name,"positive") == 1;
}

/**
 * Event handlers
 */

int nb_event = 0;

static void testEventTime(const std::vector<std::string>&)
{
  double actual_time = simgrid::s4u::Engine::get_clock();
  double expected_time = nb_event * M_PI;
  nb_event++;

  assert(actual_time <= expected_time + step_size + error && actual_time >= expected_time - error); 
  XBT_INFO("correct zero crossing detection of sin(time) at time %f",actual_time);

  if (nb_event % 2 !=0)
    simgrid::fmi::register_event(reactOnNegativeValue, testEventTime, {});
  else
    simgrid::fmi::register_event(reactOnPositiveValue, testEventTime, {});
}



int main(int argc, char *argv[])
{
  simgrid::s4u::Engine e(&argc, argv);
  simgrid::fmi::init(step_size);

  e.load_platform("../../platforms/clusters_rennes.xml");
  
  std::string fmu_uri = (argc>=2)? argv[1] : "sinus_fmu/sinus.fmu";
  simgrid::fmi::add_fmu_cs(fmu_uri, fmu_name);
  simgrid::fmi::ready_for_simulation();

  XBT_INFO("start simulation of x=sin(time) for %f seconds", max_sim_time);
  simgrid::fmi::register_event(reactOnPositiveValue, testEventTime, {});
  e.run_until(max_sim_time);
  XBT_INFO("end of simulation of sin(time), zero crossings have been correctly detected");
  
  xbt_assert(nb_event == expected_detections,
    "error! %i zero crossings have been detected whereas %i detections were expected", nb_event, expected_detections);

  return 0;
}
