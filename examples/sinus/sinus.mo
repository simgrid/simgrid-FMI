model sinus

output Real x;
output Integer positive;

equation
  x = sin(time);
  if(x >= 0) then
    positive = 1;
  else
    positive = 0;
  end if; 

end sinus;

