model monolithic_lorenz

parameter Real sigma = 10;
parameter Real rho = 28;
parameter Real beta = 8/3;

output Real x, y, z;

initial equation
  x = 1;
  y = 1;
  z = 1;

equation
  der(x) = sigma * (y - x);
  der(y) = x * (rho - z) - y;
  der(z) = x * y - beta * z;

end monolithic_lorenz;


model lorenz_x

parameter Real sigma = 10;

input Real y;
output Real x;

initial equation
 x = 1;

equation
 der(x) = sigma * (y - x);

end lorenz_x;



model lorenz_y

parameter Real rho = 28;

output Real y;
input Real x, z;

initial equation
 y = 1;

equation
 der(y) = x * (rho - z) - y;

end lorenz_y;



model lorenz_z

parameter Real beta = 8/3;

output Real z;
input Real x, y;

initial equation
 z = 1;

equation
 der(z) = x * y - beta * z;

end lorenz_z;
