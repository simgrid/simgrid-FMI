#!/bin/bash

#cd examples/lorenz

# we launch the FMU co-simulation of the Lorenz system in SimGrid
./s4u-lorenz 0.001

# we launch the monolithic simulation of the Lorenz system in OpenModelica
omc runMonolithicLorenz.mos

# we compare the result
awk -F'[;,]' 'function abs(v){return v<0 ? -v : v} NR==FNR{x[FNR]=$2; y[FNR]=$3; z[FNR]=$4;next}; (abs(x[FNR]-$2) > 0.1){print(FNR " " abs(x[FNR]-$2)); exit 1}' output.csv monolithic_lorenz/monolithic_lorenz_res.csv
