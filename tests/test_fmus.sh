#!/bin/bash
base_dir_results='fmi-cross-check/results/'
base_dir_fmus='fmi-cross-check/fmus/'
base_dir_linux64='2.0/cs/linux64/'
output_file_suffix='_out.csv'
simgrid_fmi_dir='SimGrid-FMI/2019/'

failure=0
nb_tests=0

# make the Simgrid-FMI dir results
simgridfmi_res_dir=${base_dir_results}${base_dir_linux64}${simgrid_fmi_dir} 
rm -rf "${simgridfmi_res_dir}"
mkdir -p "${simgridfmi_res_dir}"

# get all the tools to test fmus
readarray -t fmu_paths < tools_to_test.txt
fmus_dir=${base_dir_fmus}${base_dir_linux64}

# for each fmu to test
for fmu_path in "${fmu_paths[@]}"; do

  fmu=$(basename ${fmus_dir}${fmu_path})

  echo "start test of FMU ${fmu} at ${fmu_path}"
  ((nb_tests++))
  # path to write the results of the test
  res_dir="${simgridfmi_res_dir}${fmu_path}"
  mkdir -p ${res_dir}

  # start the test
  ./s4u-fmu-tester "${fmus_dir}${fmu_path}" "$fmu" &> /dev/null 
  res="$?"

  # manage success or failure
  if [ "$res" -ne "0" ] 
  then
    echo "ERROR: SimGrid-FMI co-simulation of ${fmu} at ${fmu_path} did not end properly"
    ((failure++))
    touch ${res_dir}/failed
  else
    touch ${res_dir}/passed
    mv ${fmu}${output_file_suffix} ${res_dir}
  fi
  cp README.txt ${res_dir} 
  echo "   (2) run ./SimGrid-FMI/tests/s4u-fmu-tester ${tool_dir} ${fmu}" >> ${res_dir}/README.txt
done

echo "${failure} tests failed out of ${nb_tests}"
echo "Running FMI cross-check script to test the results..."

cd 'fmi-cross-check'
python3 -m fmpy.cross_check.validate_vendor_repo --clean-up &> /dev/null
python3 -m fmpy.cross_check.validate_vendor_repo
res=$?

return_code=$((${res}+${failure}))
exit ${return_code}
