#ifndef INCLUDE_MASTER_HPP_
#define INCLUDE_MASTER_HPP_

#include "simgrid-fmi.hpp"
#include <boost/functional/hash.hpp>
#include <fmi4cpp/fmi4cpp.hpp>
#include <fstream>
#include <iostream>
#include <simgrid/kernel/resource/Model.hpp>
#include <string>
#include <unordered_map>
#include <vector>

struct port{
  std::string fmu;
  std::string name;
};

bool operator==(const port &a, const port &b);
bool operator==(const port& a, const port& b)
{
  return (a.fmu == b.fmu) && (a.name == b.name);
}

namespace std{
  using boost::hash_value;
  using boost::hash_combine;
  template<> struct hash<port>{
    size_t operator()(const port& k) const{
      std::size_t seed = 0;
      hash_combine(seed,hash_value(k.fmu));
      hash_combine(seed,hash_value(k.name));

      return seed;
    }
  };
}

namespace simgrid::fmi{

/* Coupling SimGrid -> FMU */
template <typename T> struct simgrid_fmu_connection {
  port in;
  std::function<T(std::vector<std::string>)> generateInput;
  std::vector<std::string> params;
};

class MasterFMI : public simgrid::kernel::resource::Model{

private:
  /*
   * The set of FMUs to simulate
   */
  std::unordered_map<std::string, std::unique_ptr<fmi4cpp::fmi2::cs_slave>> fmus;
  /*
   * Indicate if an FMU require an iteration (i.e. doStep(O)) to update its outputs when setting input
   */
  std::unordered_map<std::string,bool> iterate_input;

  /**
   * coupling between FMUs (nb: key=input value=output !)
   */
  std::unordered_map<port,port> couplings;
  std::vector<port> in_coupled_input;

  /**
   * coupling between SimGrid models and FMUs
   */
  std::vector<simgrid_fmu_connection<double>> real_ext_couplings;
  std::vector<simgrid_fmu_connection<int>> integer_ext_couplings;
  std::vector<simgrid_fmu_connection<bool>> boolean_ext_couplings;
  std::vector<simgrid_fmu_connection<std::string>> string_ext_couplings;

  std::vector<port> ext_coupled_input;

  bool ready_for_simulation = false;

  /**
   * last output values send to the input
   */
  std::unordered_map<port,double> last_real_outputs;
  std::unordered_map<port,int> last_int_outputs;
  std::unordered_map<port,bool> last_bool_outputs;
  std::unordered_map<port,std::string> last_string_outputs;

  double commStep;
  double current_time = 0;

  bool firstEvent       = true;
  bool externalCoupling = false;

  std::vector<std::function<void(const std::vector<std::string>&)>> event_handlers;
  std::vector<std::function<bool(const std::vector<std::string>&)>> event_conditions;
  std::vector<std::vector<std::string>> event_params;

  bool check_event_occurence();
  void manage_event_notification();
  void solve_couplings(bool firstIteration);
  bool solve_coupling(const port& in, const port& out, bool checkChange);
  void solve_external_couplings();
  void check_port_validity(const std::string& fmu_name, const std::string& port_name, const std::string& type, bool check_already_coupled);
  bool is_input_coupled(const std::string& fmu, const std::string& input_name);
  void check_not_ready_for_simulation() const;


public:
  explicit MasterFMI(const double stepSize);

  void add_fmu_cs(const std::string& fmu_uri, const std::string& fmu_name);
  void update_actions_state(double now, double delta) override;
  
  double get_real(const std::string& fmi_name, const std::string& output_name, bool checkPort=false);
  bool get_boolean(const std::string& fmi_name, const std::string& output_name, bool checkPort=false);
  int get_integer(const std::string& fmi_name, const std::string& output_name, bool checkPort=false);
  std::string get_string(const std::string& fmi_name, const std::string& output_name, bool checkPort=false);
  var_type get_var_type(const std::string& fmu_name, const std::string& var_name);

  void set_real(const std::string& fmi_name, const std::string& input_name, double value, bool simgrid_input);
  void set_boolean(const std::string& fmi_name, const std::string& input_name, bool value, bool simgrid_input);
  void set_integer(const std::string& fmi_name, const std::string& input_name, int value, bool simgrid_input);
  void set_string(const std::string& fmi_name, const std::string& input_name, const std::string& value, bool simgrid_input);
  
  double next_occurring_event(double now) override;
  
  void register_event(const std::function<bool(const std::vector<std::string>&)>& condition, const std::function<void(const std::vector<std::string>&)>& handleEvent, const std::vector<std::string>& params);
  void delete_events();
  void connect_fmu(const std::string& out_fmu_name, const std::string& output_port, const std::string& in_fmu_name, const std::string& input_port);

  void connect_real_to_simgrid(const std::function<double(std::vector<std::string>)>& generateInput,
                               const std::vector<std::string>& params, const std::string& fmu_name,
                               const std::string& input_name);
  void connect_integer_to_simgrid(const std::function<int(std::vector<std::string>)>& generateInput,
                                  const std::vector<std::string>& params, const std::string& fmu_name,
                                  const std::string& input_name);
  void connect_boolean_to_simgrid(const std::function<bool(std::vector<std::string>)>& generateInput,
                                  const std::vector<std::string>& params, const std::string& fmu_name,
                                  const std::string& input_name);
  void connect_string_to_simgrid(const std::function<std::string(std::vector<std::string>)>& generateInput,
                                 const std::vector<std::string>& params, const std::string& fmu_name,
                                 const std::string& input_name);

  void init_couplings();
};

}

#endif /* INCLUDE_MASTER_HPP_ */
