#ifndef INCLUDE_SIMGRID_PLUGINS_FMI_HPP_
#define INCLUDE_SIMGRID_PLUGINS_FMI_HPP_

#include <string>
#include <vector>
#include <simgrid/s4u.hpp>
#include <iostream>

namespace simgrid::fmi{

/**
 * Types of FMU variables
 */
enum class var_type{
  real,
  integer,
  boolean,
  string,
  unknown
};

/*
 * Initialize the FMI plugin of SimGrid.
 * This method has to be called after starting the SimGrid engine.
 * The other SimGrid-FMI methods can not be called before this one.
 * The SimGrid-FMI co-simulation master will manage the FMUs with an integration step-size and a state-events detection precision equal to communication_step.
 */
void init(double communication_step);

/*
 * Add a new FMU fmu_name located at fmu_uri to be co-simulated with SimGrid --i.e. load and initialize the FMU.
 * SimGrid will manage the co-evolution of the FMU with its own models and the distributed application.
 * SimGrid actors can then interact with the FMU.
 */
void add_fmu_cs(const std::string& fmu_uri, const std::string& fmu_name);

/*
 * Add an output-to-input ports connection between two FMUs.
 * During the co-simulation, SimGrid will update the input_port value of out_fmu_name according to the output_port value of in_fmu_name.
 * Input ports connected to other FMUs can not be set manually by SimGrid Actors.
 */
void connect_fmu(const std::string& out_fmu_name, const std::string& output_port, const std::string& in_fmu_name, const std::string& input_port);

/*
 * Get the type (i.e. real, integer, boolean, string or unknown) of an FMU variable.
 */
var_type get_var_type(const std::string& fmu_name, const std::string& var_name);

/**
 * Return the current value of the real output port (i.e. continuous) output_name of fmi_name.
 */
double get_real(const std::string& fmi_name, const std::string& output_name);

/**
 * Return the current value of the boolean output port output_name of fmi_name.
 */
bool get_boolean(const std::string& fmi_name, const std::string& output_name);

/**
 * Return the current value of the integer output port (i.e. discrete) output_name of fmi_name.
 */
int get_integer(const std::string& fmi_name, const std::string& output_name);

/**
 * Return the current value of the string output port output_name of fmi_name.
 */
std::string get_string(const std::string& fmi_name, const std::string& output_name);

/*
 * Set the value of the real input port (i.e. continuous) input_name of fmi_name.
 * Input ports connected to other FMUs can not be set using this method.
 */
void set_real(const std::string& fmi_name, const std::string& input_name, double value);

/*
 * Set the value of the boolean input port input_name of fmi_name.
 * Input ports connected to other FMUs can not be set using this method.
 */
void set_boolean(const std::string& fmi_name, const std::string& input_name, bool value);

/*
 * Set the value of the integer input port (i.e. discrete) input_name of fmi_name.
 * Input ports connected to other FMUs can not be set using this method.
 */
void set_integer(const std::string& fmi_name, const std::string& input_name, int value);

/*
 * Set the value of the string input port (i.e. continuous) input_name of fmi_name.
 * Input ports connected to other FMUs can not be set using this method.
 */
void set_string(const std::string& fmi_name, const std::string& input_name, const std::string& value);

/*
 * Define state-event and register a callback.
 * The condition boolean function determines (i.e. returns true) when the state-event occurs (e.g. return x >= 0).
 * The handle_event function will be called by the master with the params parameters when the state-event will occur.
 * Each state-event is triggered only once.
 */
void register_event(const std::function<bool(const std::vector<std::string>&)>& condition, const std::function<void(const std::vector<std::string>&)>& handleEvent, const std::vector<std::string>& params);

/*
 * Delete all the previously registered state-events.
 * The co-simulation master will stop detecting these state-events. Then no callbacks will be called.
 */
void delete_events();

/*
 * Connect the SimGrid models to the real input port (i.e. continuous) input_name of fmu_name.
 * The generate_input function is used with params to convert the state of SimGrid models into FMU input port values.
 * The SimGrid-FMI master will automatically use this function to update the FMU input port upon SimGrid models state change.
 */
void connect_real_to_simgrid(const std::function<double(std::vector<std::string>)>& generateInput,
                             const std::vector<std::string>& params, const std::string& fmu_name,
                             const std::string& input_name);

/*
 * Connect the SimGrid models to the integer input port (i.e. discrete) input_name of fmu_name.
 * The generate_input function is used with params to convert the state of SimGrid models into FMU input port values.
 * The SimGrid-FMI master will automatically use this function to update the FMU input port upon SimGrid models state change.
 */
void connect_integer_to_simgrid(const std::function<int(std::vector<std::string>)>& generateInput,
                                const std::vector<std::string>& params, const std::string& fmu_name,
                                const std::string& input_name);

/*
 * Connect the SimGrid models to the boolean input port input_name of fmu_name.
 * The generate_input function is used with params to convert the state of SimGrid models into FMU input port values.
 * The SimGrid-FMI master will automatically use this function to update the FMU input port upon SimGrid models state change.
 */
void connect_boolean_to_simgrid(const std::function<bool(std::vector<std::string>)>& generateInput,
                                const std::vector<std::string>& params, const std::string& fmu_name,
                                const std::string& input_name);

/*
 * Connect the SimGrid models to the string input port input_name of fmu_name.
 * The generate_input function is used with params to convert the state of SimGrid models into FMU input port values.
 * The SimGrid-FMI master will automatically use this function to update the FMU input port upon SimGrid models state change.
 */
void connect_string_to_simgrid(const std::function<std::string(std::vector<std::string>)>& generateInput,
                               const std::vector<std::string>& params, const std::string& fmu_name,
                               const std::string& input_name);

/*
 * Solve the initial FMUs couplings.
 * Have to be called before starting the SimGrid simulation.
 * New couplings are forbidden after calling this method.
 */
void ready_for_simulation();

extern xbt::signal<void()> on_state_change;

}

#endif /* INCLUDE_SIMGRID_PLUGINS_FMI_HPP_ */
